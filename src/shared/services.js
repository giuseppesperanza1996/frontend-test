const api_token = 'keyEC9F0usYdVoWzg';

export const getApiUrl = (model) => {
	switch(model) {
		case "lists":
			return "https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/tblWcsEyTOIf47ZuQ?api_key=" + api_token;
		case "friendRequest":
			return `https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/Friend%20request?api_key=${api_token}`;
		case "details":
			return (id) => `https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/People/${id}?api_key=${api_token}`
		default:
			return "";
	}
}